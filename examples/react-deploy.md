npm i -g netlify

netlify.cmd login

# Wersja testowa
netlify.cmd deploy 

# Wersja produkcyjna (publiczna)
netlify.cmd deploy --prod

https://programator-hosting-test.netlify.com/


# Deploy React 
https://www.netlify.com/blog/2016/07/22/deploy-react-apps-in-less-than-30-seconds/

npm install -g create-react-app

create-react-app hello-world

cd hello-world

npm run build

netlify.cmd deploy --dir build

# Backend (json)
https://www.netlify.com/blog/2018/09/13/how-to-run-express.js-apps-with-netlify-functions/
https://github.com/neverendingqs/netlify-express