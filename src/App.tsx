import React from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import { PostsPage } from './pages/Posts';
import { Route, Switch } from 'react-router-dom';
import { SinglePostPage } from './pages/SinglePost';
import { Navbar } from './components/Navbar';
import { LoginPage } from './pages/LoginPage';

const Layout: React.FunctionComponent = (props) => {
  return <div className="row">
    <div className="col">
      {props.children}
    </div>
  </div>
}
// git clone  https://bitbucket.org/ev45ive/programator-react

function App() {
  return (<>

    <Navbar/>

    <div className="container">
      <Layout>
        <Switch>
          <Route path="/" exact={true} render={() => <h1>Home</h1>} />

          <Route path="/blog" exact={true} component={PostsPage} />

          <Route path="/blog/:post_id" component={SinglePostPage} />

          <Route path="/login"  component={LoginPage} />

          <Route path="**" render={() => <h1>404 Page not found </h1>} />
        </Switch>
      </Layout>
    </div>
  </>
  );
}

export default App;
