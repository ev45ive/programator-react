import { User } from "./User";

export interface Post {
    id: number;
    title: string;

    // Optional data (possibly undefined)
    body?: string
    user?: User
    comments?: PostComment[]
}


export interface PostComment {
    id: number;
    postId:number,
    name: string;
    email: string;
    body: string;
}
