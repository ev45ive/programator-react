import React from "react";
import { Post, PostComment } from "./Post";
import { PostCard } from "../components/PostCard";
import { RouteComponentProps } from "react-router-dom";
import { CommentsList } from "../components/CommentsList";
import { CommentForm } from "../components/CommentForm";

import axios from 'axios'

type S = {
    post: Post | null
}
type P = {} & RouteComponentProps<{
    post_id: string
}>

export class SinglePostPage extends React.Component<P, S> {
    state: S = {
        post: null
    }

    componentDidMount() {
        this.fetchPost(this.props.match.params.post_id)
        console.log('hello')
    }
    // componentDidUpdate(){}
    componentWillUnmount() {
        console.log('bye bye')
    }

    fetchPost = (id: string) => {

        axios.get<Post>(`http://localhost:9000/posts/${id}`, {
            params: {
                '_expand': 'user',
                '_embed': 'comments'
            }
        })
            .then((dane) => this.setState({ post: dane.data }))
    }

    addComment = (comment: Omit<PostComment, 'id'>) => {
        axios.post(`http://localhost:9000/comments`, comment).then(data => {
            console.log(data)
            this.fetchPost(this.props.match.params.post_id)
        })

    }


    render() {
        const post = this.state.post

        return <div>
            {!post ? <div>Loading</div> : <>

                <PostCard post={post} />

                <CommentForm
                    post={post}
                    onCommentAdd={this.addComment} />

                {post.comments &&
                    <CommentsList
                        comments={post.comments} />}

            </>}

        </div>
    }
}