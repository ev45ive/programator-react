import React from "react";
import axios from 'axios'
import { User } from "./User";
import { UserContext } from "../UserContext";

type S = {
    username: string
    password: string
}
export class LoginPage extends React.Component<{}, S>{
    
    static contextType = UserContext

    state = { username: '', password: '' }

    tryLogin = () => {
        axios.get<User[]>('http://localhost:9000/users/', {
            params: {
                username: this.state.username,
                password: this.state.password
            }
        }).then(resp => {
            if (resp.data.length) {
                this.context.setUser(resp.data[0])
            }
        })
    }

    handleInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target

        switch (name) {
            case 'username': this.setState({ username: value }); break;
            case 'password': this.setState({ password: value }); break;
        }
    }

    render() {
        return <div className="col-5 mx-auto">
            <h3 className="text-center">Login</h3>

            <div className="form-group">
                <label>Login</label>
                <input type="text" className="form-control" name="username" onChange={this.handleInput} />
            </div>

            <div className="form-group">
                <label>Password</label>
                <input type="password" className="form-control" name="password" onChange={this.handleInput} />
            </div>

            <input type="button" value="Login" className="btn btn-block btn-success" onClick={this.tryLogin} />
        </div>
    }
}