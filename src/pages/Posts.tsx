import React from 'react'
import { PostCard } from '../components/PostCard'
import { Post } from './Post'

type S = {
    posts: Post[]
}

export class PostsPage extends React.Component<{}, S> {

    state: S = {
        posts: [
            { id: 123, title: 'Placki 123' },
            { id: 234, title: 'Placki 234' },
            { id: 345, title: 'Placki 345' },
        ]
    }

    componentDidMount() {
        this.loadPosts()
    }

    loadPosts = () => {

        fetch('http://localhost:9000/posts?_limit=10&_expand=user')
            .then(r => r.json())
            .then((dane: Post[]) => {
                this.setState({ posts: dane })
            })
    }


    render() {
        return <div>
            {/* <button onClick={this.loadPosts} style={{ float: 'right' }}>Refresh</button> */}

            <div>
                {this.state.posts.map(post =>
                    <PostCard post={post} readmore={true} key={post.id} />
                )}
            </div>
        </div>
    }

}