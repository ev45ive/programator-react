import React from "react";
import { Post, PostComment } from "../pages/Post";
import { User } from "../pages/User";
import { UserContext } from "../UserContext";

type P = {
    onCommentAdd(comment: Omit<PostComment, 'id'>): void,
    post: Post,
}
type S = {
    commentText: string
}

export class CommentForm extends React.Component<P, S>{

    static contextType = UserContext

    state: S = { commentText: '' }

    handleButtonClick = () => {
        if (!this.context.user) { return }

        const comment: Omit<PostComment, 'id'> = {
            postId: this.props.post.id,
            body: this.state.commentText,
            name: this.context.user?.name,
            email: this.context.user?.email
        }

        this.props.onCommentAdd(comment)
        
        this.setState({ commentText: '' })
    }

    handleInput = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
        const value = event.target.value
        if (value.length <= 170) {
            this.setState({ commentText: value })
        }
    }

    render() {
        return <div className="mt-3">
            <h5> Add Comment</h5>

            <div className="input-group">
                <textarea rows={3} cols={30} className="form-control"
                    onChange={this.handleInput}
                    value={this.state.commentText}
                />

                <button className="btn btn-info" onClick={this.handleButtonClick}>
                    Add
                </button>
            </div>
            <span>{this.state.commentText.length} / 170</span>
        </div>
    }
}