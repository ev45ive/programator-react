import React from "react"
import { PostComment } from "../pages/Post"

// https://getbootstrap.com/docs/4.4/
// ... components/media-object/#media-list

type P = {
    comments: PostComment[]
}

export const CommentsList = (props: P) => <div className="mt-3 card">
    <div className="card-body">

        <ul className="list-unstyled">

            {props.comments.map(comment =>
                <li className="media mb-3" key={comment.id}>
                    <div className="media-body">
                        <h6 className="mt-0 mb-1">
                            {comment.name}
                        </h6>
                        {comment.body}
                    </div>
                </li>
            )}
        </ul>
    </div>
</div>