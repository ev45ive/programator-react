import React, { useState } from "react";
import { Post } from "../pages/Post";
import { UserAvatar } from "./UserAvatar";
import { Link } from "react-router-dom";

type P = {
    post: Post
    readmore?: boolean
}

export const PostCard = (props: P) => {
    const [likes, setLikes] = useState(0)

    const inc = () => setLikes(likes + 1)
    const dec = () => likes > 0 && setLikes(likes - 1)
    
    return (
        <div className="card">
            <div className="card-body">
                <h5 className="card-title">
                    {props.post.user && <UserAvatar user={props.post.user} />}
                    {props.post.title}

                </h5>
                <div>
                    <button onClick={inc}>+</button>
                    <span className="px-3"> {likes} </span>
                    <button onClick={dec}>-</button>
                </div>


                {props.readmore ?
                    <Link to={'/blog/' + props.post.id}
                        className="float-right">
                        Read more ...
                    </Link> : props.post.body
                }
            </div>
        </div>
    )
}