import React from "react";
import { Link, NavLink } from "react-router-dom";
import { UserContext } from "../UserContext";

// https://getbootstrap.com/docs/4.4/components/navbar/#nav

export const Navbar = () => <nav className="navbar 
                                            navbar-expand 
                                            navbar-dark 
                                            bg-dark mb-3">
    <div className="container">

        <Link className="navbar-brand" to="/">Auctions</Link>

        <div className="collapse navbar-collapse">
            <ul className="navbar-nav">
                <li className="nav-item">
                    <NavLink className="nav-link" to="/" exact={true} 
                             activeClassName="placki active">Home</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/blog">Blog</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/auctions">Auctions</NavLink>
                </li>
            </ul>
        </div>

        <UserContext.Consumer>{
            context => context.user ? <div className="text-light">
                   
                Witaj {context.user.name},

               <span onClick={()=>context.setUser(null)}> Wyloguj </span>
        
            </div> : <Link to="/login" className="text-light"> Zaloguj </Link>
        }</UserContext.Consumer>
    </div>
</nav>