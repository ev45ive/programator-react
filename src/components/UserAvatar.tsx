import React from "react"
import { User } from "../pages/User"

type P = {
    user: User
}

export const UserAvatar = (props: P) => {
    return <div>
        <h5>
            <span style={{
                width: '30px',
                height: '30px',
                background: '#ccc',
                borderRadius: '50%',
                display: 'inline-block'
            }}> </span>
            
            <span style={{
                verticalAlign: 'top',
                paddingLeft: '10px'
            }}>{props.user.name}</span>
        </h5>
    </div >
}